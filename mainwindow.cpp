#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>

#include "checksum.h"
#include "aes.h"

//#include <QtSerialPort/QSerialPortInfo>
//#include <QtSerialPort/QSerialPort>

static const uint8_t serial_key[] = {0x73, 0x36, 0x76, 0x39, 0x79, 0x2F, 0x42, 0x3F, 0x45, 0x28, 0x48, 0x2B, 0x4D, 0x62, 0x51, 0x65,
                                     0x54, 0x68, 0x57, 0x6D, 0x5A, 0x71, 0x34, 0x74, 0x37, 0x77, 0x21, 0x7A, 0x25, 0x43, 0x26, 0x46};

static const uint8_t serial_iv[AES_BLOCKLEN] = {0x42, 0x3F, 0x45, 0x28, 0x47, 0x2B, 0x4B, 0x62,
                                                0x50, 0x65, 0x53, 0x68, 0x56, 0x6D, 0x59, 0x71};

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_serial(new QSerialPort(this))
{
    ui->setupUi(this);

    uart_fillport_param();
    uart_fillport_info();
    uart_settings();
    uart_init_combo_command();
    uart_openport();

    ui->textBrowser->setReadOnly(false);

    // initialize progress bar
    progressbar = new QProgressBar();
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(100);
    ui->progressBar->hide();
    progressval = 0;

    // connection initial state is OFF/disconnected
    ui->checkBox->setEnabled(true);
    ui->checkBox->setChecked(false);
    ui->label_2->setText("DISCONNECTED");

    // channel initialized to normal
    ui->checkBox_2->setEnabled(true);
    ui->checkBox_2->setChecked(false);
    channelstate = false;

    ui->pushButton_3->setEnabled(false);

    connect(m_serial, &QSerialPort::readyRead, this, &MainWindow::uart_readdata);

}

MainWindow::~MainWindow()
{   
    if (m_serial->isOpen())
        m_serial->close();

    ui->label_2->setText("DISCONNECTED");

    delete m_serial;
    delete ui;
}

void MainWindow::uart_data_display(QString str)
{
    ui->textBrowser->append(str);
}

/*
void MainWindow::setcombotext(QString str)
{
    ui->comboBox->addItem(str);
}
*/

void MainWindow::uart_init_combo_command(void)
{
    ui->comboBox->addItem("Encrypt a Message");
    ui->comboBox->addItem("Decrypt a Message");
    ui->comboBox->addItem("Request Temperature Logs");
    ui->comboBox->addItem("Request ALS Logs");
    ui->comboBox->addItem("Request All Logs");
}

void MainWindow::uart_fillport_param(void)
{
    // baudrate
    ui->comboBox_3->addItem(QStringLiteral("115200"), QSerialPort::Baud115200);
    ui->comboBox_3->addItem(QStringLiteral("38400"), QSerialPort::Baud38400);
    ui->comboBox_3->addItem(QStringLiteral("19200"), QSerialPort::Baud19200);
    ui->comboBox_3->addItem(QStringLiteral("9600"), QSerialPort::Baud9600);

    // databits
    ui->comboBox_5->addItem(QStringLiteral("8"), QSerialPort::Data8);
    ui->comboBox_5->addItem(QStringLiteral("7"), QSerialPort::Data7);
    ui->comboBox_5->addItem(QStringLiteral("6"), QSerialPort::Data6);
    ui->comboBox_5->addItem(QStringLiteral("5"), QSerialPort::Data5);

    // parity
    ui->comboBox_4->addItem(tr("None"), QSerialPort::NoParity);
    ui->comboBox_4->addItem(tr("Even"), QSerialPort::EvenParity);
    ui->comboBox_4->addItem(tr("Odd"), QSerialPort::OddParity);
    ui->comboBox_4->addItem(tr("Mark"), QSerialPort::MarkParity);
    ui->comboBox_4->addItem(tr("Space"), QSerialPort::SpaceParity);
}

void MainWindow::uart_fillport_info()
{
    ui->comboBox_2->clear();

    const auto portinfo = QSerialPortInfo::availablePorts();

    // display available commport to combo box
    for (const QSerialPortInfo &info : portinfo)
        ui->comboBox_2->addItem(info.portName());
}

void MainWindow::uart_settings(void)
{
    m_currentsettings.name = ui->comboBox_2->currentText();

    if (ui->comboBox_3->currentText() == 1)
    {
        m_currentsettings.baudrate = ui->comboBox_3->currentText().toInt();
    }
    else
    {
        m_currentsettings.baudrate = static_cast<QSerialPort::BaudRate>
                                     (ui->comboBox_3->itemData(ui->comboBox_3->currentIndex()).toInt());
    }
    m_currentsettings.stringbaudrate = QString::number(m_currentsettings.baudrate);

    m_currentsettings.databits = static_cast<QSerialPort::DataBits>
                                 (ui->comboBox_5->itemData(ui->comboBox_5->currentIndex()).toInt());
    m_currentsettings.stringdatabits = ui->comboBox_5->currentText();

    m_currentsettings.parity = static_cast<QSerialPort::Parity>
                               (ui->comboBox_4->itemData(ui->comboBox_4->currentIndex()).toInt());
    m_currentsettings.stringparity = ui->comboBox_4->currentText();
}

int MainWindow::uart_openport(void)
{
    m_serial->setPortName(m_currentsettings.name);
    m_serial->setBaudRate(m_currentsettings.baudrate);
    m_serial->setDataBits(m_currentsettings.databits);
    m_serial->setParity(m_currentsettings.parity);

    if (m_serial->open(QIODevice::ReadWrite))
    {
        showStatusMessage(tr("Connected to %1 : %2, %3, %4")
                          .arg(m_currentsettings.name)
                          .arg(m_currentsettings.stringbaudrate)
                          .arg(m_currentsettings.stringdatabits)
                          .arg(m_currentsettings.stringparity));

        portstate = true;
    }
    else
    {
        QMessageBox::critical(this, tr("Error"), m_serial->errorString());

        showStatusMessage(tr("Open error"));
        return 1;
    }

    return 0;
}

void MainWindow::uart_closeport(void)
{
    if (m_serial->isOpen())
        m_serial->close();
}

void MainWindow::showStatusMessage(QString str)
{
    ui->statusbar->setStyleSheet("color: blue");
    ui->statusbar->showMessage(str);
}

// start button
void MainWindow::on_pushButton_clicked()
{    
    int cmd;
    DataFrameStruct frame;

    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(100);
    progressval = 0;
    ui->progressBar->hide();

    memset(&frame, 0, sizeof(frame));

    cmd = ui->comboBox->currentText().toInt();
    switch(cmd)
    {
        case 0: frame.CmdType = COMMANDTYPEENC;  // encrypt
        break;
        case 1: frame.CmdType = COMMANDTYPEDEC;
        break;
        case 2: frame.CmdType = COMMANDTYPEREQTEMP;
        break;
        case 3: frame.CmdType = COMMANDTYPEREQALSL;
        break;
        case 4: frame.CmdType = COMMANDTYPEREQALLL;
        break;
        default:
            while(1);   // for debug
    }

    // compose packet
    frame.StartOfRame = STARTOFFRAME;
    frame.EndOfFrame = ENDOFFRAME;
    frame.PktLen = sizeof(frame);
    frame.CmdChnl = CHANNELNORMAL;

    // add the payload
    QString text = ui->textBrowser->toPlainText();
    QByteArray payload = text.toLocal8Bit();
    frame.PayloadLen = text.count();

    for (int i = 0; (i < text.count()) && (i < 64); i++)
    {
        frame.Payload[i] = payload[i];
    }

    // encode channel command here
    if (channelstate == true)
    {
        // call encryption here
        struct AES_ctx encrypt_ctx;

        AES_init_ctx(&encrypt_ctx, (const uint8_t *)&serial_key);
        AES_init_ctx_iv(&encrypt_ctx, (const uint8_t *)&serial_key, (const uint8_t *)&serial_iv);

        // encrypt the frame now
        AES_CBC_encrypt_buffer(&encrypt_ctx, frame.Payload, 64);
    }

    // compute crc16
    QByteArray packet_no_crc((char *)&frame, sizeof(frame));
    uint16_t frame_crc16 = crc_16((unsigned char *)packet_no_crc.data(), (size_t)packet_no_crc.size());
    frame.Crc = frame_crc16;

    // convert frame to QByteArray as requirement by built-in qt-uart
    QByteArray packet((char *)&frame, sizeof(frame));
    uart_writedata(packet);

#if 0
    // for testing only, decrypt the data
    if (channelstate == true)
    {
        // call decryption here
        struct AES_ctx decrypt_ctx;

        AES_init_ctx(&decrypt_ctx, (const uint8_t *)&serial_key);
        AES_init_ctx_iv(&decrypt_ctx, (const uint8_t *)&serial_key, (const uint8_t *)&serial_iv);

        // decrypt the frame now
        AES_CBC_decrypt_buffer(&decrypt_ctx, frame.Payload, 64);
    }
#endif

    return;
}

// select file button
void MainWindow::on_pushButton_3_clicked()
{
    QFile file(QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Text Files (*.txt)")));
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream stream(&file);
    QString text = stream.readAll();
    ui->textBrowser->append(text);
    file.close();
}

void MainWindow::uart_writedata(QByteArray data)
{
    int count, xmitcnt = 0;
    double pct;
    QByteArray transmit;

    ui->progressBar->show();

    count = data.count();
    pct = count/100;
    if (pct < 1)
        pct = 1;
    while (count != 0)
    {
        progressval = progressval + pct;
        transmit[xmitcnt] = data[xmitcnt];
        count--;
        xmitcnt++;
        ui->progressBar->setValue(progressval);
    }

    ui->progressBar->setValue(100);
    m_serial->write(data);
}

void MainWindow::uart_readdata(void)
{
    RspFrameStruct RspFrame;
    QString payload;
    QString rspdisplay;

    memset(&RspFrame, 0,sizeof(RspFrame));

    const QByteArray data = m_serial->readAll();

    RspFrame.StartOfFrame[0] = data[0];
    RspFrame.StartOfFrame[1] = data[1];
    RspFrame.PktLen[0] = data[2];
    RspFrame.PktLen[1] = data[3];
    RspFrame.Crc[0] = data[4];
    RspFrame.Crc[1] = data[5];
    RspFrame.Crc[2] = data[6];
    RspFrame.Crc[3] = data[7];

    RspFrame.RspType = data[8];

    RspFrame.CmdChnl = data[9];

    RspFrame.PayloadLen[0] = data[10];
    RspFrame.PayloadLen[1] = data[11];

    // payload
    for (int i = 0; i < RspFrame.PayloadLen[1]; i++)
    {
        payload.append(data[12+i]);
        RspFrame.Payload[i] = data[12+i];
    }

    RspFrame.EndOfFrame[0] = data[78];
    RspFrame.EndOfFrame[1] = data[79];

    uint16_t rspframecrc16 = crc_16((unsigned char *)&RspFrame, sizeof(RspFrame));

    if ((rspframecrc16 >> 8) == RspFrame.Crc[2])
    {
        if (rspframecrc16 == RspFrame.Crc[3])
        {
            // crc16 is correct
        }
        else
        {
            while(1);   // for debug
        }
    }
    else
    {
        while(1);   // for debug
    }

    switch (RspFrame.RspType)
    {
        case 0x46:
            rspdisplay.append("Encryption Command:");
            rspdisplay.append(payload);
            break;
        case 0x47:
            rspdisplay.append("Decryption Command:");
            rspdisplay.append(payload);
            break;
        case 0x48:
            rspdisplay.append("Request Temperature Command:");
            rspdisplay.append(payload);
            break;
        case 0x49:
            rspdisplay.append("Request ALS Log:");
            rspdisplay.append(payload);
            break;
        case 0x4A:
            rspdisplay.append("Request All Logs:");
            rspdisplay.append(payload);
            break;
        default:
            while (1);
    }

    ui->textBrowser_2->setText(rspdisplay);
}

void MainWindow::on_checkBox_stateChanged(int arg1)
{
    Q_UNUSED(arg1)
    if (ui->checkBox->checkState())
    {
        if (portstate == true)
        {
            ui->label_2->setText("CONNECTED");
        }
        else
        {
            if (uart_openport() == 0)
                ui->label_2->setText("CONNECTED");
        }
    }
    else
    {
        uart_closeport();
        ui->label_2->setText("DISCONNECTED");
        portstate = false;
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    // change settings button
    uart_closeport();
    uart_settings();
    uart_openport();
}

void MainWindow::on_checkBox_2_stateChanged(int arg1)
{
    Q_UNUSED(arg1)

    // channel
    if (ui->checkBox_2->checkState())
    {
        channelstate = true;
    }
    else
    {
        channelstate = false;
    }
}

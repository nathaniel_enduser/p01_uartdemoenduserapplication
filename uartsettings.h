#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QString>

#define BAUDRATE_115200     115200
#define BAUDRATE_57600       57600
#define BAUDRATE_38400       38400
#define BAUDRATE_19200       19200
#define BAUDRATE_9600         9600

#endif // MAINWINDOW_H

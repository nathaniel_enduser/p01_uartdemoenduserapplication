#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileSystemModel>
#include <QtSerialPort/QSerialPortInfo>
#include <QtSerialPort/QSerialPort>

#include <QtEndian>

#define STARTOFFRAME        0xaa
#define COMMANDTYPEENC      0x45
#define COMMANDTYPEDEC      0x44
#define COMMANDTYPEREQTEMP  0x54
#define COMMANDTYPEREQALSL  0x41
#define COMMANDTYPEREQALLL  0x4C
#define CHANNELSECURED      0x53
#define CHANNELNORMAL       0x4E
#define ENDOFFRAME          0xee

#define RESPTYPEENC         0x46
#define RESPTYPEDEC         0x47
#define RESPTYPERQSTTEMP    0x48
#define RESPTYPERQSTALS     0x49
#define RESPTYPERQSTALL     0x4A

typedef struct DataFrameStruct
{
    quint16_be  StartOfRame;    // Start of frame
    quint16_be  PktLen;         // Packet len including from SOF to EOF
    quint32_be  Crc;            // CRC16
    uint8_t     CmdType;        // Command Type
    uint8_t     CmdChnl;        // Channel command
    quint16_be  PayloadLen;     // data len
    uint8_t     Payload[64];    // each packet you send it in 64 bytes, either 2 bytes try to padd it to 64 bytes.
    uint8_t     Reserved[2];    // this will be reserved for counter
    quint16_be  EndOfFrame;     // end of frame
} DataFrameStruct;

typedef struct RspFrameStruct
{
    uint8_t     StartOfFrame[2]; // Start of frame
    uint8_t     PktLen[2];      // Packet len including from SOF to EOF
    uint8_t     Crc[4];         // CRC16
    uint8_t     RspType;        // Command Type
    uint8_t     CmdChnl;        // Channel command
    uint8_t     PayloadLen[2];     // data len
    uint8_t     Payload[64];    // each packet you send it in 64 bytes, either 2 bytes try to padd it to 64 bytes.
    uint8_t     Reserved[2];    // this will be reserved for counter
    uint8_t     EndOfFrame[2];  // end of frame
} RspFrameStruct;

class QProgressBar;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    struct Settings
    {
        QString                 name;
        qint32                  baudrate;
        QString                 stringbaudrate;
        QSerialPort::DataBits   databits;
        QString                 stringdatabits;
        QSerialPort::Parity     parity;
        QString                 stringparity;
        QSerialPort::StopBits   stopbits;
        QString                 stringstopbits;
        bool                    localechoenabled;
    };    

    Settings settings() const;

    void    uart_fillport_param(void);
    void    uart_fillport_info(void);
    void    uart_settings(void);
    int     uart_openport(void);
    void    uart_closeport(void);
    void    uart_writedata(QByteArray data);
    void    uart_readdata(void);
    void    showStatusMessage(QString str);

    void    uart_data_display(QString str);
    void    uart_init_combo_command(void);
    void    uart_init_port_option(void);

    int     progressval;
    bool    portstate;
    bool    channelstate;

private slots:
    void    on_pushButton_3_clicked();
    void    on_pushButton_clicked();

    void on_checkBox_stateChanged(int arg1);

    void on_pushButton_2_clicked();

    void on_checkBox_2_stateChanged(int arg1);

private:
    Ui::MainWindow  *ui;
    Settings        m_currentsettings;
    QSerialPort     *m_serial = nullptr;
    QProgressBar    *progressbar;
};
#endif // MAINWINDOW_H
